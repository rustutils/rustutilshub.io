---
title:  'Rust Utilities'
...

These are some useful utilities written in Rust.

| Name | Description | Link |
| ---- | ----------- | ---- |
| `ripgrep` | Recursively searches directories for a regex pattern. | [github](https://github.com/BurntSushi/ripgrep) |
| Alacritty | Terminal emulator that uses GPU acceleration to be fast. | [github](https://github.com/alacritty/alacritty) |
| fselect | Find files with SQL-like queries. | [github](https://github.com/jhspetersson/fselect) |
| coreutils | Cross-platform coreutils written in Rust. | [github](https://github.com/uutils/coreutils) |
| rr | Debugger that allows recording executions. | [website](https://rr-project.org) |
| Sequoia PGP | Modern, more intuitive implementation of GPG | [website](https://sequoia-pgp.org) |
| rage | Rust implementation of age, a simple, secure and modern encryption tool with small explicit keys, no config options, and UNIX-style composability | [github](https://github.com/str4d/rage) |
| bat | Like `cat`, but better and with syntax highlighting. | [github](https://github.com/sharkdp/bat) |
| dust | A more intuitive version of du in rust. | [github](https://github.com/bootandy/dust) |
| fd | A simple, fast and user-friendly alternative to 'find'. | [github](https://github.com/sharkdp/fd) |
| skim | Fuzzy Finder in rust! | [github](https://github.com/lotabout/skim) |
| exa | A modern replacement for ls. | [github](https://the.exa.website) |
| procs | A modern replacement for ps written in Rust. | [github](https://github.com/dalance/procs) |
| sd | ntuitive find & replace CLI (sed alternative) | [github](https://github.com/chmln/sd) |
| hyperfine | A command-line benchmarking tool. | [github](https://github.com/sharkdp/hyperfine) |
| zenith | Zenith - sort of like top or htop but with zoom-able charts, network, and disk usage. | [github](https://github.com/bvaisvil/zenith) |
| tokei | Count your code, quickly (replacement for cloc). | [github](https://github.com/XAMPPRocky/tokei) |
| xsv | A fast CSV command line toolkit written in Rust. | [github](https://github.com/BurntSushi/xsv) |
| delta | Diff viewer with syntax highlighting. | [github](https://github.com/dandavison/delta) |
| ffsend | Share files from the command line. Firefox Send client. | [github](https://github.com/timvisee/ffsend) |
| sekey | Use Touch ID / Secure Enclave for SSH Authentication! | [github](https://github.com/sekey/sekey) |
| librespot | Spotify | [github](https://github.com/librespot-org/librespot) |
| flamegraph | Generate flamegraphs for executables | [github](https://github.com/flamegraph-rs/flamegraph) |
| xi editor | Editor with backend written in Rust. | [github](https://github.com/xi-editor/xi-editor) |

# Related Work

- GNU Coreutils: [coreutils/coreutils](https://github.com/coreutils/coreutils)
- Busybox: <https://busybox.net/>
- OpenBSD utils: <https://github.com/openbsd/src/tree/master/bin>
